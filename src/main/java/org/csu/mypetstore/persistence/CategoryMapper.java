package org.csu.mypetstore.persistence;

import org.springframework.stereotype.Repository;
import org.csu.mypetstore.domain.Category;
import java.util.List;

@Repository
public interface CategoryMapper {

    List<Category> getCategoryList();

    Category getCategory(String categoryId);

    List<String> getAllCategoryId();
}


