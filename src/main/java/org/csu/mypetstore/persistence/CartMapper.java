package org.csu.mypetstore.persistence;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.CartItem;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Param;
import java.math.BigDecimal;
import java.util.List;

@Repository
public interface CartMapper {
    void insertItem(CartItem item);

    CartItem getItemById(@Param("s")String s,@Param("userid")String userid);

    List<CartItem> getAllItem(Account account);

    void updateCart(CartItem item);

    void removeCartItem(CartItem item);

    void clearCart(Account account);

    void increaseQuantity(CartItem item);

    void updateQuantity(@Param("id")String id,@Param("quantity")int quantity,@Param("userid")String userid);

//    BigDecimal getTotal(Account account);

    int getSize(Account account);

}
