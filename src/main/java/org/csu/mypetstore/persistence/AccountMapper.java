package org.csu.mypetstore.persistence;

import org.apache.ibatis.annotations.Param;
import org.csu.mypetstore.domain.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountMapper {

    //通过用户名获取账号，用于注册和登陆
    Account getAccountByUsername(String username);

    //通过用户名和密码来获取账号，用于登陆
    Account getAccountByUsernameAndPassword(Account account);

    List<String> getUserId();

    List<Account> getAccountList();

    //注册用户
    void insertAccount(Account account);

    void insertProfile(Account account);

    void insertSignon(Account account);

    //修改用户信息
    void updateAccount(Account account);

    void updateProfile(Account account);

    void updateSignon(Account account);

    //删除用户
    void deleteAccount(Account account);

    void delProfile(Account account);

    void delSignOn(Account account);

    //重置密码
    void resetPassword(@Param("username")String username, @Param("password") String password);


}
