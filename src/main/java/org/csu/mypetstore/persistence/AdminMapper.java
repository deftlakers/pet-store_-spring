package org.csu.mypetstore.persistence;

import org.csu.mypetstore.domain.Admin;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminMapper {
    Admin getAdmin(Admin admin);

    //注册admin
    void insertAdmin(Admin admin);


}
