package org.csu.mypetstore.service;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Admin;
import org.csu.mypetstore.persistence.AccountMapper;
import org.csu.mypetstore.persistence.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManagementService {
    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private AccountService accountService;

    public Admin getAdmin(Admin admin){ return adminMapper.getAdmin(admin);}

    public void insertAdmin(Admin admin){
        adminMapper.insertAdmin(admin);
    }
}
