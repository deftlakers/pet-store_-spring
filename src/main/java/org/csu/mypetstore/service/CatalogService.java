package org.csu.mypetstore.service;

import org.csu.mypetstore.domain.Category;
import org.csu.mypetstore.domain.Item;
import org.csu.mypetstore.domain.Product;
import org.csu.mypetstore.persistence.CategoryMapper;
import org.csu.mypetstore.persistence.ItemMapper;
import org.csu.mypetstore.persistence.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class CatalogService {

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ItemMapper itemMapper;

    public Category getCategory(String categoryId){
        return categoryMapper.getCategory(categoryId);
    }

    public Product getProduct(String productId) {
        return productMapper.getProduct(productId);
    }

    //找到匹配categoryId的Product
    public List<Product> getProductListByCategory(String categoryId) {
        return productMapper.getProductListByCategory(categoryId);
    }


    //按照关键字查询
    public List<Product> searchProductList(String keyword) {
        return productMapper.searchProductList("%" + keyword.toLowerCase() + "%");
    }

    public List<String> getName(){
        return productMapper.getName();
    }

    public void updateInventoryQuantity(Map<String, Object> param){
        itemMapper.updateInventoryQuantity(param);
    }

    public int getInventoryQuantity(String itemId){
        return itemMapper.getInventoryQuantity(itemId);
    }

    public List<Item> getItemListByProduct(String productId){
        return itemMapper.getItemListByProduct(productId);
    }

    public Item getItem(String itemId){
        return itemMapper.getItem(itemId);
    }

    public boolean isItemInStock(String itemId) {
        return itemMapper.getInventoryQuantity(itemId) > 0;
    }

    public List<Item> getAllItems() {
        return itemMapper.getAllItems();
    }

    public void insertNewItem(Item item) {itemMapper.insertItem(item);}

    public List<String> getAllProductId() {return productMapper.getAllProductId();}

    public List<Product> getProductList(){ return productMapper.getProductList();}

    public void updateProduct(Product p){productMapper.updateProduct(p);}

    public void updateItem(Item item){itemMapper.updateItem(item);}

    public void insertProduct(Product product) {productMapper.insertProduct(product);}

    public List<String> getAllCategoryId(){return categoryMapper.getAllCategoryId();}

//    public void deleteItem(Item item, String id){itemMapper.deleteItemById(id);}
}
