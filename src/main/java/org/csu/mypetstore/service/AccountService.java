package org.csu.mypetstore.service;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.persistence.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountMapper accountMapper;

    public Account getAccount(String username) {
        return accountMapper.getAccountByUsername(username);
    }

    public Account getAccount(String username, String password) {
        Account account = new Account();
        account.setUsername(username);
        account.setPassword(password);
        return accountMapper.getAccountByUsernameAndPassword(account);
    }

    public void insertAccount(Account account) {
        accountMapper.insertAccount(account);
        accountMapper.insertProfile(account);
        accountMapper.insertSignon(account);
    }

    public void updateAccount(Account account) {
        accountMapper.updateAccount(account);
        accountMapper.updateProfile(account);

        if (account.getPassword() != null && account.getPassword().length() > 0) {
            accountMapper.updateSignon(account);
        }
    }

    public List<String> getUserId(){ return accountMapper.getUserId();}

    public List<Account> getAccountList(){return accountMapper.getAccountList();}

    public void insertProfile(Account account){accountMapper.insertProfile(account);}

    public void insertSignon(Account account){accountMapper.insertSignon(account);}

    public void updateProfile(Account account){accountMapper.updateProfile(account);}

    public void updateSignon(Account account){accountMapper.updateSignon(account);}

    public void deleteAccount(Account account){accountMapper.deleteAccount(account);}

    //重置密码
    public void resetPassword(String username,String password){
        accountMapper.resetPassword(username,password);
    }
}
