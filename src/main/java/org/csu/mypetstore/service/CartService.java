package org.csu.mypetstore.service;

import org.apache.ibatis.annotations.Param;
import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.CartItem;
import org.csu.mypetstore.domain.Category;
import org.csu.mypetstore.domain.Item;
import org.csu.mypetstore.persistence.AccountMapper;
import org.csu.mypetstore.persistence.CartMapper;
import org.csu.mypetstore.persistence.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CartService {

    @Autowired
    private CatalogService CatalogService;

    @Autowired
    public CartMapper cartMapper;

    @Transactional
    public void insertItem(CartItem item){
         cartMapper.insertItem(item);
    }

    public CartItem getItemById(String s,String userid){
        CartItem cartItem =  cartMapper.getItemById(s,userid);

        cartItem.setItem(CatalogService.getItem(cartItem.getItemid()));

        return cartItem;

    }

    public List<CartItem> getAllItem(Account account){

        List<CartItem> list  = cartMapper.getAllItem(account);
        for(int i = 0 ; i < list.size();i++){
            list.get(i).setItem(CatalogService.getItem(list.get(i).getItemid()));
        }
        return list;

    }


    public void updateCart(CartItem item){
        cartMapper.updateCart(item);
    }


    public void removeCartItem(CartItem item){
        cartMapper.removeCartItem(item);
    }


    public void clearCart(Account account){
        cartMapper.clearCart(account);
    }

    public void increaseQuantity(CartItem item){
        cartMapper.increaseQuantity(item);
    }

    public void updateQuantity(String id,int quantity,String userid){
        cartMapper.updateQuantity(id,quantity,userid);
    }
    public  BigDecimal getTotal(Account account){
        BigDecimal subTotal = new BigDecimal("0");
        List<CartItem> list = getAllItem(account);
        for(int i = 0;i < list.size();i++){
            CartItem cartItem = list.get(i);
            Item item = cartItem.getItem();
            BigDecimal listPrice = item.getListPrice();
            BigDecimal quantity = new BigDecimal(String.valueOf(cartItem.getQuantity()));
            subTotal = subTotal.add(listPrice.multiply(quantity));
        }
        return subTotal;
    }
    public int getSize(Account account){
        return cartMapper.getSize(account);
    }
}
