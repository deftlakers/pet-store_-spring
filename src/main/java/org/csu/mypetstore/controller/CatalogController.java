package org.csu.mypetstore.controller;

import com.alibaba.fastjson.JSON;
import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Category;
import org.csu.mypetstore.domain.Item;
import org.csu.mypetstore.domain.Product;
import org.csu.mypetstore.service.AccountService;
import org.csu.mypetstore.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.Console;
import java.io.PrintWriter;
import java.util.List;


@Controller
@RequestMapping("/catalog")
public class CatalogController {
    public static int itemidcount = 20;
    @Autowired
    private CatalogService catalogService;

    @Autowired
    private AccountService accountService;

    @GetMapping("/viewMain")
    public String viewMain(){
        return "catalog/Main";
    }

    @GetMapping("/viewCategory")
    public String viewCategory(String categoryId, Model model){

        Category c = catalogService.getCategory(categoryId);
        List<Product> list = catalogService.getProductListByCategory(categoryId);


        model.addAttribute("category",c);
        model.addAttribute("productList",list);
        return "catalog/Category";
    }

    @GetMapping("/adminViewItem")
    @ResponseBody
    public List<Item> adminViewItem() {

        return catalogService.getAllItems();
    }

    @ResponseBody
    @GetMapping("/viewAllProductId")
    public List<String> viewAllProductId(Model model) {
        List<String> productIdList = catalogService.getAllProductId();
        model.addAttribute("list", productIdList);
        return productIdList;
    }

    @ResponseBody
    @GetMapping("/viewAllCategoryId")
    public List<String> viewAllCategoryId(Model model) {
        List<String> categoryIdList = catalogService.getAllCategoryId();
        model.addAttribute("list", categoryIdList);
        return categoryIdList;
    }

    @GetMapping("/viewProduct")
    public String viewProduct(String productId,Model model){

        Product p = catalogService.getProduct(productId);
        List<Item> list = catalogService.getItemListByProduct(productId);

        model.addAttribute("product",p);
        model.addAttribute("itemList",list);

        return "catalog/Product";
    }

    @GetMapping("/viewItem")
    public String viewItem(String itemId,Model model){

        Item item = catalogService.getItem(itemId);
        Product product = item.getProduct();

        model.addAttribute("item",item);
        model.addAttribute("product",product);
        return "catalog/Item";
    }

    @ResponseBody
    @PostMapping("/getProductInfo")
    public String getProductInfo(HttpServletRequest request){
        String caId = request.getParameter("caId");

        List<Product> list;

        list = catalogService.getProductListByCategory(caId);

        String res = "[";
        for(int i = 0 ; i < list.size();i++){
            res += "{\"proId\":\""+list.get(i).getProductId()+"\",\"name\":\""+list.get(i).getName()+"\"}";
            if(i != list.size()-1){
                res+=",";
            }else res+="]";
        }

        return res;
    }

    @PostMapping("autoComplete")
    public Object autoComplete(HttpServletRequest request){
        String code = request.getParameter("code");
        String result = "";

        if(code.equals("0")){
            AccountService accountService = new AccountService();
            List<String> a = accountService.getUserId();
            result = JSON.toJSONString(a);
        }else if(code.equals("1")){

            List<String> a = catalogService.getName();
            result = JSON.toJSONString(a);
        }
        return result;

    }

    @RequestMapping("/newItem")
    public String newItem(Item item) {
        catalogService.insertNewItem(item);
        return "management/Main";
    }

    @GetMapping("/getProductList")
    @ResponseBody
    public List<Product> getProductList(){
        return catalogService.getProductList();
    }

    @ResponseBody
    @PostMapping("/insertProduct")
    public void insertProduct(String productId,String categoryId,String name,String filename,String description){


        String file= filename.replace("C:\\fakepath\\","");
        String temp = "<img src=\"../images/"+ file + "\">"+description;

        Category c = catalogService.getCategory(categoryId);
        Product p = new Product();
        p.setProductId(productId);
        p.setName(name);
        p.setDescription(temp);
        p.setCategoryId(categoryId);
        catalogService.insertProduct(p);

    }






}
