package org.csu.mypetstore.controller;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.CartItem;
import org.csu.mypetstore.domain.Order;
import org.csu.mypetstore.service.CartService;
import org.csu.mypetstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.*;

@Controller
public class PayController {

    @Autowired
    private CartService cartService;
    @Autowired
    private OrderService orderService;
    private final String APP_ID = "2021000117635880";
    private final String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDKAR0NsVBQYxvMHHzR6CIT9TVNML9gAryPPbX6QiWSCxj+DCYMCyagQCttHGc9qUAhI397B714GEUOBpi/FWj4Oplw7ZNEsogwuEe9JI6OX0LSIjjwUJWossJXPSXNTw0TXhmlOPwzgUFagUBqRefX16IbgDUEOhaWKApXK0R3SyRqZ0PXURBVVMlfe+60+BArYTYXPiM7i+SKQ9H5Ch0EMh+tJMNpgfXnEmY22eTf1So1/K34LvN/v1Z5CcHdp7T5MKmMlSVFx0s9xs/qahUQPovEGhC8cBP/A3soNg2TwontiuJTjfqBb8Xm6DsjKrwE7nMpL9XZG/SERffbiqh5AgMBAAECggEAShFKc+DWEAOnB5pScizpgePhHWKIaU3XkbYQ6agjWn9TNKODt9rG2O9ufN1zRXqvlvcLITNfV4+eklv82eOhz5K8/FlALGpB4YtQUpv64V0whRgjRIpntamF9LLpD3Sy2K0iPiC86K22JtdJpS3oqo3YVLkXt2sNKRWZZQjqW8q8huoUTdn18LINgcMVcukIT6sw/y+xTjrZC91UTt9NTgG2LVRiT5MtLIm81Wg/9jFwY1Pvlx+QpRHhKd6YI13VCjEBBtFOOiYRs7NbkGLee3fl8uSTLd2OV5f1dyOBLVCx3h49HGVuc3f4GRSmkwlQqb48NFACGAX7pa4Jx0LgOQKBgQDxpj7Js/q7ykJ9o9ApAWCk+9fXh2fArypdRF++/614cTJW+ZqGT3pFEOldI1TBLjzF0D6Z8sC2vY4c4Mvn9cfws6N+BmjlI0+AbNAmV/iBjgvqqNR9MZF+p8ffy2b/WVTw+7Z9/ZNjP9mFnqZTf3QDPyHT+NN6b7vPiqAuMVnkQwKBgQDWACa8/8t6CTp/2euSrAs1JPlJUKfeAA2sRnDd8IYVH6JEVUB6jPa+dF/HcH2wVaRWy6uYc87t+x5CcIy/9GGl41kwo742Kz4+k/jzNfJYOtD3Ms9jJnbnGobfRs9Zuv594GDamkLVNNzCdHtvxXb5rtI0akqasTMbB079cpKykwKBgHrjQCrzDB9TzFx4YfN4b9129Vz1zx199t1XWBmZjG4joqX4aUzEPjnBT+u9e+yBxwYR0MQZX0ePPiYRa6CarQMLV6lPOSjNvDBjJbdaJVY9FnJPSUAhZQwz5sE4fzh8BWZHm5aWzc07OzNoas0rk6PTBzD06Rbqm2f/51WWY1BhAoGBAJoxT7g8gusyfREj/sYW6phnRKtbzyh5V2C6/JYduS69NVkRKD1lFmCST3TPi/CjNJ9JbyGQyBfXKa1rlS62MimqD1ipkuNK8zV5vxXW2ABUmL6UwLNLQr8v8OAuEDcyMi6WnpLENcWTqkD+VvZpDFnJajJUJ98gMJeYA4VAqYy7AoGBAI8laOgCDuJMZhUFCMLLjGHVNZqoUBc8a29xfs6lWKftFSJGDIrNawN8zMwfzYY9ldY018daixGcenwtpm0aNEzNQQwoMr1pnL9ojKHudMqbUrW/N1oLSTBLsPv5Gt6nn95H7XX5LOQgjHx57YjlPn5iPvKyN8r6/fM4SpPgm1WO";
    private final String CHARSET = "UTF-8";
    private final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAleAx+xp5HqHU8mB9KYmq8vUbnDm+I1eH9y6X7sL0kMT7wfqwKgvd53dPJgZ5DHaScCTDQzPOKkVHIiOZ7yi6ZvwBi1CjKiM/yN6DSBPOI/P8enR/VOBqKoF+MCFtR72GNUM4R7hanmioaMdrJjcczJRd9d7V9yCplMHvm0mbqfNInkyzmsfzp57fkzWo4gei4ZN7IUBhN5Sl1mzVzeMzoJbedBqOjtL3+PKL+uPB6PzM7vbS3SOxY/OmLWlbJqmAj+bkAqM7W1M3r07kcve19f1CxLI4BbZO8Zaw9qNCt7nKVNl/ez13uDUXT2hC7vX8iVDGCoYWAblAPp5z9iszmQIDAQAB";
    //这是沙箱接口路径,正式路径为https://openapi.alipay.com/gateway.do
    private final String GATEWAY_URL ="https://openapi.alipaydev.com/gateway.do";
    private final String FORMAT = "JSON";
    //签名方式
    private final String SIGN_TYPE = "RSA2";
    //支付宝异步通知路径,付款完毕后会异步调用本项目的方法,必须为公网地址
    private final String NOTIFY_URL = "http://127.0.0.1/notifyUrl";
    //支付宝同步通知路径,也就是当付款完毕后跳转本项目的页面,可以不是公网地址
    private final String RETURN_URL = "http://localhost/returnUrl";



    @RequestMapping("alipay")
    public void alipay(HttpServletResponse httpResponse) throws IOException {

        SecureRandom r= new SecureRandom();
        //实例化客户端,填入所需参数
        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL, APP_ID, APP_PRIVATE_KEY, FORMAT, CHARSET, ALIPAY_PUBLIC_KEY, SIGN_TYPE);
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //在公共参数中设置回跳和通知地址
        request.setReturnUrl(RETURN_URL);
        request.setNotifyUrl(NOTIFY_URL);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        //生成随机Id
        String out_trade_no = UUID.randomUUID().toString();
        //付款金额，必填
        String total_amount =Integer.toString(r.nextInt(5)+10);
        //订单名称，必填
        String subject ="奥迪A8 2021款 A8L 60 TFSl quattro豪华型";
        //商品描述，可空
        String body = "尊敬的会员欢迎购买奥迪A8 2021款 A8L 60 TFSl quattro豪华型";
        request.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String form = "";
        try {
            form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        httpResponse.setContentType("text/html;charset=" + CHARSET);
        httpResponse.getWriter().write(form);// 直接将完整的表单html输出到页面
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
    }


    @RequestMapping(value = "/returnUrl", method = RequestMethod.GET)
    public String returnUrl(HttpServletRequest request, HttpServletResponse response)
            throws IOException, AlipayApiException {
        System.out.println("=================================同步回调=====================================");

        // 获取支付宝GET过来反馈信息
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("utf-8"), "utf-8");
            params.put(name, valueStr);
        }

        System.out.println(params);//查看参数都有哪些
        boolean signVerified = AlipaySignature.rsaCheckV1(params, ALIPAY_PUBLIC_KEY, CHARSET, SIGN_TYPE); // 调用SDK验证签名
        //验证签名通过
        if(signVerified){
            // 商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");

            // 支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

            // 付款金额
            String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");

            System.out.println("商户订单号="+out_trade_no);
            System.out.println("支付宝交易号="+trade_no);
            System.out.println("付款金额="+total_amount);

            //支付成功，修复支付状态
            //payService.updateById(Integer.valueOf(out_trade_no));

            HttpSession session = request.getSession();
            Account account = (Account)session.getAttribute("account");
            Order order = (Order) session.getAttribute("order");
            List<CartItem> cart = cartService.getAllItem(account);

            if (order != null) {

                cart = null;
                orderService.insertOrder(order);
                session.setAttribute("order", order);
                //清空购物车
                cartService.clearCart(account);

                session.setAttribute("cart", cart);

                session.setAttribute("message", "Thank you, your order has been submitted.");

                //APP.log(session,request,"查看订单");

                return "order/ViewOrder";

            } else {
                session.setAttribute("message", "An error occurred processing your order (order was null).");
                //APP.log(session,request,"错误：订单为空");
                return "common/Error";
            }

        }else{
            return "no";//跳转付款失败页面
        }

    }



}


