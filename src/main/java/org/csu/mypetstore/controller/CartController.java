package org.csu.mypetstore.controller;

//import com.sun.xml.internal.ws.resources.HttpserverMessages;
import org.csu.mypetstore.domain.Account;

import org.csu.mypetstore.domain.CartItem;
import org.csu.mypetstore.domain.Item;
import org.csu.mypetstore.persistence.CartMapper;
import org.csu.mypetstore.service.CartService;
import org.csu.mypetstore.service.CatalogService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/cart")
public class CartController {

    HttpSession session;
    Account account;

    @Autowired
    private CartService cartService;
    @Autowired
    private CatalogService catalogService;

    @GetMapping("viewCart")
    public String viewCart(HttpServletRequest request, Model model){




//        if(account != null){
//            HttpServletRequest httpRequest= request;
//            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
//                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());
//
//            LogService logService = new LogService();
//            String logInfo = logService.logInfo(" ") + strBackUrl + " 查看购物车 " + cart;
//            logService.insertLogInfo(account.getUsername(), logInfo);
//        }

        session = request.getSession();

        Account account = (Account)session.getAttribute("account");

        List<CartItem> cart = cartService.getAllItem(account);
        BigDecimal subTotal = cartService.getTotal(account);
        model.addAttribute("cartList",cart);
        session.setAttribute("cart",cart);
        session.setAttribute("totalCost",subTotal);

        return "cart/Cart";

    }

    @GetMapping("/insertToCart")
    public String insertToCart(HttpServletRequest request,Model model){


        session = request.getSession();
        account = (Account)session.getAttribute("account") ;
        String workingItemId = request.getParameter("workingItemId");

        if(account == null){
//            APP.log(session,request,"添加物品失败");
            session.setAttribute("message","请先登录");
            return "common/Error";
        }
        else{
            CartItem cartItem = new CartItem();

            Item item = catalogService.getItem(workingItemId);

            cartItem.setUserid(account.getUsername());
            cartItem.setItem(item);
            cartItem.setQuantity(1);
            cartItem.setInStock(catalogService.isItemInStock(workingItemId));


            cartService.insertItem(cartItem);

            List<CartItem> cart = cartService.getAllItem(account);
            BigDecimal subTotal = cartService.getTotal(account);
            model.addAttribute("cartList",cart);
            session.setAttribute("cartList",cart);
            session.setAttribute("totalCost",subTotal);
            //APP.log(session,request,"添加物品 "+cartItem.getItem().getItemId()+" 到购物车");
            return "cart/Cart";
        }
    }



    @ResponseBody
    @GetMapping("/removeItem")
    public Object removeItem(HttpServletRequest request){

        HttpSession session = request.getSession();
        String workingItemId = request.getParameter("itemId");

        Account account = (Account)session.getAttribute("account");

        CartItem item = cartService.getItemById(workingItemId,account.getUsername());

        cartService.removeCartItem(item);
        BigDecimal subTotal = cartService.getTotal(account);

        Map<String,Object> map = new HashMap<>();
        map.put("subTotal",subTotal);
        return map;


    }

    @ResponseBody
    @GetMapping("/cartChange")
    public Object cartChange(HttpServletRequest request){
        HttpSession session = request.getSession();

        Account account = (Account)session.getAttribute("account");

        String itemId = request.getParameter("itemid");
        int newQuantity = Integer.parseInt(request.getParameter("quantity"));
        cartService.updateQuantity(itemId,newQuantity,account.getUsername());

        List<CartItem> cart = cartService.getAllItem(account);
        BigDecimal subTotal = cartService.getTotal(account);

        CartItem item = cartService.getItemById(itemId,account.getUsername());

        String res = "{\"total\":\""+item.getTotal()+"\",\"subTotal\":\""+subTotal+"\"}";

        Map<String,Object> map = new HashMap<>();
        map.put("total",item.getTotal());
        map.put("subTotal",subTotal);
        return map;

    }

}
