package org.csu.mypetstore.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
//import com.sun.org.apache.xpath.internal.operations.Bool;
import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.util.List;

@Controller
@RequestMapping("/account")
public class AccountController {


    @Autowired
    private AccountService accountService;


    private Account account;

    private HttpSession session;

    //登录表单
    @GetMapping("/signonForm")
    public String signonForm(){

        return "account/SignonForm";
    }

    //验证用户名和密码
    @PostMapping("/signOn")
    public String signOn(HttpServletRequest request){

        session = request.getSession();

        String username = request.getParameter("username");
        String password = request.getParameter("password");


        account = accountService.getAccount(username, password);
        session.setAttribute("account", account);

        if(account != null){

            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                    + request.getContextPath() + request.getServletPath() + "?" + (request.getQueryString());

//            LogService logService = new LogService();
//            String logInfo = logService.logInfo(" ") + strBackUrl + " 用户登录";
//            logService.insertLogInfo(account.getUsername(), logInfo);
        }

//        //获得输入的验证码值
//        String value1=request.getParameter("vCode");
//        /*获取图片的值*/
//        String value2=(String)session.getAttribute("checkcode");
//        Boolean isSame = false;
//        /*对比两个值（字母不区分大小写）*/
//        if(value2.equalsIgnoreCase(value1)){
//            isSame = true;
//        }
        Boolean isSame = true;

        session.setAttribute("messageSignOn", null);
        if (account == null || !isSame){
            if(!isSame){
                session.setAttribute("messageSignOn", "Invalid Verification Code.   Signon failed.");
            }else{
                session.setAttribute("messageSignOn", "Invalid username or password.  Signon failed.");
            }
            session.setAttribute("account", null);
            return "account/SignonForm";
        }else{
            account.setPassword(null);
            return "catalog/Main";

        }

    }

    //登出
    @GetMapping("/signOff")
    public String signOff(HttpServletRequest request){
        session = request.getSession();
        account = (Account)session.getAttribute("account");

        if(account != null){

            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                    + request.getContextPath() + request.getServletPath() + "?" + (request.getQueryString());

//            LogService logService = new LogService();
//            String logInfo = logService.logInfo(" ") + strBackUrl + " 退出账号";
//            logService.insertLogInfo(account.getUsername(), logInfo);
        }

        account = null;

        session.setAttribute("account", account);

        return "catalog/Main";
    }

   //转到编辑账号表单
    @GetMapping("toEditAccount")
    public String toEditAccount(HttpServletRequest request){

        session = request.getSession();
        account = (Account)session.getAttribute("account");

        if(account != null){
//            HttpServletRequest httpRequest = request;
//            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
//                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());
//
//            LogService logService = new LogService();
//            String logInfo = logService.logInfo(" ") + strBackUrl + " 跳转到编辑账号信息界面";
//            logService.insertLogInfo(account.getUsername(), logInfo);
        }

        return "account/EditAccountForm";

    }

    //管理员编辑账号
    @PostMapping("editAccount")
    public String editAccount(HttpServletRequest request){

        session = request.getSession();
        account = (Account) session.getAttribute("account");

        String username = account.getUsername();
        String password = request.getParameter("password");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String address1 = request.getParameter("address1");
        String address2 = request.getParameter("address2");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String zip = request.getParameter("zip");
        String country = request.getParameter("country");
        String languagePreference = request.getParameter("languagePreference");
        String favouriteCategoryId = request.getParameter("favouriteCategoryId");
        String listOption = request.getParameter("listOption");
        String bannerOption = request.getParameter("bannerOption");

        account.setUsername(username);
        account.setPassword(password);
        account.setFirstName(firstName);
        account.setLastName(lastName);
        account.setEmail(email);
        account.setPhone(phone);
        account.setAddress1(address1);
        account.setAddress2(address2);
        account.setCity(city);
        account.setState(state);
        account.setZip(zip);
        account.setCountry(country);
        account.setLanguagePreference(languagePreference);
        account.setFavouriteCategoryId(favouriteCategoryId);

        if(listOption.equals("on")) account.setListOption(true);
        else account.setListOption(false);

        if(bannerOption.equals("on")) account.setBannerOption(true);
        else account.setBannerOption(false);


        accountService.updateAccount(account);

        session.setAttribute("account", account);

//        if(account != null){
//            HttpServletRequest httpRequest= request;
//            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
//                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());
//
//            LogService logService = new LogService();
//            String logInfo = logService.logInfo(" ") + strBackUrl + " 账号信息更改";
//            logService.insertLogInfo(account.getUsername(), logInfo);
//        }

        return "account/EditAccountForm";

    }

    //注册表单
    @GetMapping("viewRegister")
    public String viewRegister(HttpServletRequest request){

        return "account/NewAccountForm";
    }


    @ResponseBody
    @GetMapping("userIsExist")
    public Object userIsExist(String username){

        Account result = accountService.getAccount(username);

        JSONObject object = new JSONObject();
        if(username.equals("")){
            object.put("code",3);
        }else if(result != null){
            object.put("code",1);
        }
        else {
            object.put("code",3);

        }
        return object;

    }

    @ResponseBody
    @GetMapping("/getAccountList")
    public List<Account> getAccountList(){
        List<Account> list = accountService.getAccountList();
        return list;
    }


    //管理员修改用户密码
    @PostMapping("/editPassword")
    @ResponseBody
    public String editPassword(String username,String password){
        accountService.resetPassword(username, password);
        return "success";
    }

}
