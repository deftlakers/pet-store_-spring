package org.csu.mypetstore.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.csu.mypetstore.domain.*;
import org.csu.mypetstore.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/management")
public class ManagementController {


    @Autowired
    private ManagementService managementService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private CatalogService catalogService;


    @GetMapping("/viewLogin")
    public String viewLogin(){
        return "management/Login";
    }

    @GetMapping("/viewRegister")
    public String viewRegister(){
        return "management/Register";
    }


    @PostMapping("/register")
    public String register(String adminId,String password){

        String str = MD5Util.string2MD5(password);
        Admin admin = new Admin();
        admin.setAdminId(adminId);
        admin.setPassword(str);
        Admin a = managementService.getAdmin(admin);
        if(a == null){
            managementService.insertAdmin(admin);
            return "management/RegisterSuc";
        }else{

            return "management/Register";
        }
    }


    @PostMapping("/login")
    public String login(String adminId, String password, Model model, HttpServletRequest request){

        HttpSession session = request.getSession();
        Admin adminTemp = new Admin();
        adminTemp.setAdminId(adminId);
        adminTemp.setPassword(password);
        Admin admin = managementService.getAdmin(adminTemp);
        if(admin != null){


        }
        Boolean isSame = true;

        model.addAttribute("messageSignOn", null);

        if (admin == null || !isSame){
            if(!isSame){
                model.addAttribute("messageSignOn", "Invalid Verification Code.   Signon failed.");
            }else{
                model.addAttribute("messageSignOn", "Invalid username or password.  Signon failed.");
            }
            model.addAttribute("admin", null);
            return "management/Temp";
        }else{
            admin.setPassword(null);
            session.setAttribute("admin",admin);
            return "management/Main";
        }



    }

    @ResponseBody
    @PostMapping("/editAccount")
    public void editAccount(String username,String email,String firstName,String lastName,String status,String addr1,String addr2,
                String city,String state,String zip,String country,String phone){
            Account account = new Account();
            account.setPhone(phone);
            account.setZip(zip);
            account.setState(state);
            account.setCity(city);
            account.setAddress2(addr2);
            account.setAddress1(addr1);
            account.setStatus(status);
            account.setLastName(lastName);
            account.setFirstName(firstName);
            account.setEmail(email);
            account.setUsername(username);
            account.setCountry(country);
            accountService.updateAccount(account);
    }

    @ResponseBody
    @PostMapping("deleteAccount")
    public void deleteAccount(String username){
        Account account = new Account();
        account.setUsername(username);
        accountService.deleteAccount(account);
    }

    @ResponseBody
    @GetMapping("/viewAccount")
    public String viewAccount(Model model){
        List<Account> list = accountService.getAccountList();

        model.addAttribute("accountList",list);
        return "s";

    }


    @GetMapping("/viewNewAccount")
    public String viewNewAccount(){
        return "management/newAccount";
    }

    @PostMapping("/newAccount")
    public void new0Account(String username,String email,String firstName,String lastName,String status,String addr1,String addr2,
                             String city,String state,String zip,String country,String phone){
        Account account = new Account();
        account.setPhone(phone);
        account.setZip(zip);
        account.setState(state);
        account.setCity(city);
        account.setAddress2(addr2);
        account.setAddress1(addr1);
        account.setStatus(status);
        account.setLastName(lastName);
        account.setFirstName(firstName);
        account.setEmail(email);
        account.setUsername(username);
        account.setCountry(country);

        accountService.insertAccount(account);
    }

    @GetMapping("/viewChangePassword")
    public String viewChangePassword(){
        return "/management/changePassword";
    }

    @ResponseBody
    @GetMapping("/isPasswordSame")
    public Object isPasswordSame(String username,String oldPassword,String password,String repeatPassword){
        JSONObject object = new JSONObject();

        Account a = accountService.getAccount(username,oldPassword);
        if(a == null){
            object.put("m",0);
            object.put("msg","原密码错误");
        }
        else{
            object.put("m",1);
            object.put("msg","原密码正确");
        }
        if(password.equals(repeatPassword)){
            object.put("code",1);
        }else{
            object.put("code",0);
        }
        return object;
    }


    @ResponseBody
    @PostMapping("/changePassword")
    public void changePassword(String username,String password,String repeatPassword){

        Account account = new Account();
        account.setUsername(username);
        account.setPassword(password);
        accountService.updateSignon(account);

    }

    @PostMapping("/editOrder")
    public void editOrder(String value,String field,String id){

        Order order = orderService.getOrder(Integer.parseInt(id));

        switch (field){
            case "username":
                order.setUsername(value);
                break;
            case "orderDate":
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date = sf.parse(value);
                    order.setOrderDate(date);
                }catch(Exception e){

                }
                break;
            case "shipAddress1" :
                order.setShipAddress1(value);
                break;
            case "shipAddress2" :
                order.setShipAddress2(value);
                break;
            case "shipCity" :
                order.setShipCity(value);
                break;
            case "shipState" :
                order.setShipState(value);
                break;
            case "shipZip" :
                order.setShipZip(value);
                break;
            case "shipCountry" :
                order.setShipCountry(value);
                break;
            case "billAddress1" :
                order.setBillAddress1(value);
                break;
            case "billAddress2" :
                order.setBillAddress2(value);
                break;
            case "billCity" :
                order.setBillCity(value);
                break;
            case "billState" :
                order.setBillState(value);
                break;
            case "billZip" :
                order.setBillZip(value);
                break;
            case "billCountry" :
                order.setBillCountry(value);
                break;
            case "courier" :
                order.setCourier(value);
                break;
            case "totalPrice" :
                BigDecimal bigDecimal = new BigDecimal(value);
                order.setTotalPrice(bigDecimal);
                break;
            case "billToFirstName" :
                order.setBillToFirstName(value);
                break;
            case "billToLastName" :
                order.setBillToLastName(value);
                break;
            case "shipToFirstName" :
                order.setShipToFirstName(value);
                break;
            case "shipToLastName" :
                order.setShipToLastName(value);
                break;
            case "creditCard" :
                order.setCreditCard(value);
                break;
            case "expiryDate" :
                order.setExpiryDate(value);
                break;
            case "cardType" :
                order.setCardType(value);
                break;
            case "locale" :
                order.setLocale(value);
                break;
            case "status" :
                order.setStatus(value);
                break;
                default: break;

        }

        orderService.updateOrder(order);

    }

    @PostMapping("/deleteOrder")
    public void deleteOrder(String id){
        orderService.deleteOrder(Integer.parseInt(id));
    }

    @ResponseBody
    @PostMapping("/editProduct")
    public void editProduct(String productId,String categoryId,String name,String des){

        Product p = catalogService.getProduct(productId);
        p.setCategoryId(categoryId);
        p.setName(name);
        p.setDescription(des);
        catalogService.updateProduct(p);
    }

    @ResponseBody
    @PostMapping("/editPicture")
    public void editPicture(String productId,String des,String filename){

        Product p = catalogService.getProduct(productId);
        String res = p.getDescription();
        int index = res.indexOf('>');
        String file= filename.replace("C:\\fakepath\\","");
        String temp = "<img src=\"../images/"+ file + "\">"+res.substring(index+1,res.length());
        p.setDescription(temp);
        catalogService.updateProduct(p);
    }

    @ResponseBody
    @PostMapping("/editItem")
    public void editItem(String data, String id){
        Item item = new Item();
        item = catalogService.getItem(id);
        BigDecimal cost = new BigDecimal(data);
        item.setListPrice(cost);
    }






}
