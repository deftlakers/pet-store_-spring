package org.csu.mypetstore.controller;


import org.csu.mypetstore.domain.Order;
import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.CartItem;
import org.csu.mypetstore.service.CartService;
import org.csu.mypetstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {

    private Account account;
    private Order order;
    HttpSession session;

    @Autowired
    private CartService cartService;
    @Autowired
    private OrderService orderService;
    private List<CartItem> cart;

    @GetMapping("/newOrderForm")
    public String newOrderForm(HttpServletRequest request){
        HttpSession session = request.getSession();
        account = (Account) session.getAttribute("account");
        List<CartItem> cart = new ArrayList<>();

        if (account == null){
            session.setAttribute("message", "You must sign on before attempting to check out.  Please sign on and try checking out again.");

            return "/account/SignonForm";

        } else if(cart != null){
            order = new Order();
            order.initOrder(account, cart);
            session.setAttribute("order", order);

            Account account = (Account)session.getAttribute("account");

            //APP.log(session,request,"确认订单");

            return "/order/NewOrderForm";

        }else{
            session.setAttribute("message", "An order could not be created because a cart could not be found.");

            Account account = (Account)session.getAttribute("account");

            //APP.log(session,request,"更改地址");

            return "common/Error";

        }
    }
    @PostMapping("conFirmOrderForm")
    public String conFirmOrderForm(HttpServletRequest request){


        HttpSession session = request.getSession();
        order = (Order)session.getAttribute("order");
        account = (Account)session.getAttribute("account");

        return "order/ConfirmOrder";

    }

    @GetMapping("viewOrder")
    public String viewOrder(HttpServletRequest request){
        session = request.getSession();
        account = (Account)session.getAttribute("account");
        order = (Order) session.getAttribute("order");
        cart = cartService.getAllItem(account);

        if (order != null) {

            cart = null;
            orderService.insertOrder(order);
            session.setAttribute("order", order);
            //清空购物车
            cartService.clearCart(account);

            session.setAttribute("cart", cart);

            session.setAttribute("message", "Thank you, your order has been submitted.");

            //APP.log(session,request,"查看订单");

            return "order/ViewOrder";

        } else {
            session.setAttribute("message", "An error occurred processing your order (order was null).");
            //APP.log(session,request,"错误：订单为空");
            return "common/Error";
        }
    }

    @GetMapping("viewOrderList")
    public String viewOrderList( HttpServletRequest request){

        HttpSession session = request.getSession();
        Account account = (Account)session.getAttribute("account");
        List<Order> orderList = orderService.getOrdersByUsername(account.getUsername());

        session.setAttribute("orderList", orderList);
        //APP.log(session,request,"查看订单");

        return "order/ListOrders";
    }

    @ResponseBody
    @GetMapping("/getOrderList")
    public List<Order> getOrderList(){
        return orderService.getOrderList();


    }

    @PostMapping("Export")
    public void exportOrder(HttpServletRequest request,String orderId){
        session = request.getSession();
        order = new Order();
        int id = Integer.parseInt(orderId);
        order = orderService.getOrder(id);
//        order = (Order) session.getAttribute("order");
        if(order.getStatus() == "p"){
            orderService.exportOrder(order);
            session.setAttribute("message","export successfully");
        }
        else if(order.getStatus() == "xp"){
            session.setAttribute("message","Error, the order has been experted");
        }
    }


}
