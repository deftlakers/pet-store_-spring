<%--
  Created by IntelliJ IDEA.
  User: 李良才
  Date: 2020/11/16
  Time: 17:42
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="../common/IncludeTop.jsp"%>
<script>
    $(document).ready(function(){
        $("#confirmShip").click(function(){

            console.log($('#shipAddForm').serialize());

            $.ajax({
                type    : "POST",
                url     : "shippingAddress",
                data    : {da : $('#shipAddForm').serialize()},
                success : function (data) {

                }
            })
        })

    });
</script>

<style type="text/css">
    *{margin:0px;padding:0px;}
    .tabbox{margin:10px;}
    .tabbox ul{list-style:none;display:table;}
    .tabbox ul li{float:left;width:150px;line-height:30px;padding-left:8px;border:1px solid #98F5FF;margin-right:-1px;cursor:pointer;}
    .tabbox ul li.active{background-color:#00C5CD;color:white;font-weight:bold;}
    .tabbox .content{border:1px solid #aaccff;padding:10px;}
    .tabbox .content>div{display:none;}
    .tabbox .content>div.active{display:block;}
</style>

<div class="tabbox">
   <ul>
   <li class="active">Order</li>
   <li >Ship address</li>
   </ul>

   <div class="content">
      <div class = "active">
          <div id="Catalog">
              <form action="conFirmOrderForm" method="post">
                  <table>
                      <tr>
                          <th colspan=2>Payment Details</th>
                      </tr>
                      <tr>
                          <td>Card Type:</td>
                          <td>
                              <select>
                                  <option value="Visa" selected="selected">Visa</option>
                                  <option value="MasterCard">MasterCard</option>
                                  <option value="American Express">American Express</option>
                              </select>
                          </td>
                      </tr>
                      <tr>
                          <td>Card Number:</td>
                          <td>
                              <input type="text" name="creditCard" value="999999999999">* Use a fake number!
                          </td>
                      </tr>
                      <tr>
                          <td>Expiry Date (MM/YYYY):</td>
                          <td>
                              <input type="text" name="expiryDate" value="12/03"/>
                          </td>
                      </tr>

                      <tr>
                          <th colspan=2>Billing Address</th>
                      </tr>
                      <tr>
                          <td>First name:</td>
                          <td>
                              <input type="text" name="billToFirstName" value="${sessionScope.order.billToFirstName}" />
                          </td>
                      </tr>
                      <tr>
                          <td>Last name:</td>
                          <td>
                              <input type="text" name="billToLastName" value="${sessionScope.order.billToLastName}" />
                          </td>
                      </tr>
                      <tr>
                          <td>Address 1:</td>
                          <td>
                              <input type="text" size="40" name="billAddress1" value="${sessionScope.order.billAddress1}" />
                          </td>
                      </tr>
                      <tr>
                          <td>Address 2:</td>
                          <td>
                              <input type="text" size="40" name="billAddress2" value="${sessionScope.order.billAddress2}" />
                          </td>
                      </tr>
                      <tr>
                          <td>City:</td>
                          <td>
                              <input type="text" name="billCity" value="${sessionScope.order.billCity}" />
                          </td>
                      </tr>
                      <tr>
                          <td>State:</td>
                          <td>
                              <input type="text" size="4" name="billState" value="${sessionScope.order.billState}" />
                          </td>
                      </tr>
                      <tr>
                          <td>Zip:</td>
                          <td>
                              <input type="text" size="10" name="billZip" value="${sessionScope.order.billZip}" />
                          </td>
                      </tr>
                      <tr>
                          <td>Country:</td>
                          <td>
                              <input type="text" size="15" name="billCountry" value="${sessionScope.order.billCountry}" />
                          </td>
                      </tr>

                  </table>
                  <div style="margin:0 auto;width:200px;background-color: #4d92af;
    color: white;
    padding: 8px 15px;
    border: none;
    border-radius: 4px;">
                      <input type="submit" name="newOrder" value="Continue"/>
                  </div>

              </form>
          </div><%--cata2end--%>
      </div>

      <div>
          <div id="Catalog1">
              <form id = "shipAddForm" method="post">
                  <center>
                  <table align ="center">
                      <tr>
                          <th colspan=2>Shipping Address</th>
                      </tr>

                      <tr>
                          <td>First name:</td>
                          <td><input type="text" name="shipToFirstName" value="${sessionScope.order.shipToFirstName}"/></td>
                      </tr>
                      <tr>
                          <td>Last name:</td>
                          <td><input type="text" name="shipToLastName" value="${sessionScope.order.shipToLastName}"/></td>
                      </tr>
                      <tr>
                          <td>Address 1:</td>
                          <td><input type="text" size="40" name="shipAddress1" value="${sessionScope.order.shipAddress1}"/></td>
                      </tr>
                      <tr>
                          <td>Address 2:</td>
                          <td><input type="text" size="40" name="shipAddress2" value="${sessionScope.order.shipAddress2}"/></td>
                      </tr>
                      <tr>
                          <td>City:</td>
                          <td><input type="text" name="shipCity" value="${sessionScope.order.shipCity}"/></td>
                      </tr>
                      <tr>
                          <td>State:</td>
                          <td><input type="text" size="4" name="shipState" value="${sessionScope.order.shipState}"/></td>
                      </tr>
                      <tr>
                          <td>Zip:</td>
                          <td><input type="text" size="10" name="shipZip" value="${sessionScope.order.shipZip}"/></td>
                      </tr>
                      <tr>
                          <td>Country:</td>
                          <td><input type="text" size="15" name="shipCountry" value="${sessionScope.order.shipCountry}"/></td>
                      </tr>

                  </table>

<%--                  <input type="submit" name="newOrder" value="Continue"/>--%>
                      <div style="margin:0 auto;width:200px;background-color: #4d92af;
                        color: white;
                        padding: 8px 15px;
                        border: none;
                        border-radius: 4px;">
                          <input type = "button" id ="confirmShip" value="Confirm"/>
                      </div>

                  </center>
              </form>
          </div><%--cata--%>


      </div>
    </div>
</div>
</body>


<script type="text/javascript">
    $(function ()
    {
        $(".tabbox li").click(function ()
        {
            //获取点击的元素给其添加样式，讲其兄弟元素的样式移除
            $(this).addClass("active").siblings().removeClass("active");
            //获取选中元素的下标
            var index = $(this).index();
            $(this).parent().siblings().children().eq(index).addClass("active")
                .siblings().removeClass("active");
        });
    });

</script>

<%@ include file="../common/IncludeBottom.jsp"%>
