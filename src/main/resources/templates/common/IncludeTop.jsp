
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true" language="java" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<link rel="StyleSheet" href="css/jpetstore.css" type="text/css" media="screen" />

	<meta name="generator" content="HTML Tidy for Linux/x86 (vers 1st November 2002), see www.w3.org" />
	<title>MyPetStore</title>
	<meta content="text/html; charset=windows-1252" http-equiv="Content-Type" />
	<meta http-equiv="Cache-Control" content="max-age=0" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="Pragma" content="no-cache" />

	<script type="text/javascript" src="js/jquery.js"></script>
	<!-- 引入jqueryUI的js文件 -->
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<!-- 引入jQueryUI的css文件 -->
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">

    <style>
        .ok {color : green}
        .not {color : red}
    </style>


</head>

<body>

<div id="Header">

	<div id="Logo">
		<div id="LogoContent">
			<a href="main"><img src="images/logo.jpg" id="logoImg"/></a>
		</div>
	</div>

	<div id="Menu">
		<div id="MenuContent">
			<a href="viewCart"><img align="middle" name="img_cart"
									src="images/cart.gif" id="cartImg" /></a> <img align="middle"src="images/separator.gif" />




			<c:if test="${sessionScope.account == null}">
			<a href="viewLogin">Sign In</a> <img align="middle" src="images/separator.gif" />

			</c:if>

			<c:if test="${sessionScope.account != null}">
			<c:if test="${!sessionScope.isLogin}">
				<a href="viewLogin">Sign In</a> <img align="middle" src="images/separator.gif" />
			</c:if>
			</c:if>

			<c:if test="${sessionScope.account != null}">
				<c:if test="${sessionScope.isLogin}">

					<a href="signOut">Sign Out</a>
					<img align="middle" src="images/separator.gif" />

					<a href = "toEditAccount">My Account</a>
				</c:if>
			</c:if>

			 <img align="middle" src="images/separator.gif" />

		</div>
	</div>

	<div id="Search">
		<div id="SearchContent">
			<form action="searchProduct" method="post">
				<input id = "keyword"  type="text" name="keyword" size="14" />
				<input id = "searchId" type="submit" name="searchProducts" value="Search" />

				<script>
					var b = [];
					$.ajax({
						url : "autoComplete",
						type : "post",
						data : { code : 1},
						dataType : "json",
						success: function( data ) {
							var i = 0;
							while (i  < data.length ){
								b[i] = data[i];
								i++;
							}

						}
					});
					$('#keyword').autocomplete({
						source : b
					});
				</script>


			</form>
		</div>
	</div>

	<div id="QuickLinks">
		<a href="viewCategory?categoryId=FISH">
			<img src="images/sm_fish.gif" />
		</a>
		<img src="images/separator.gif" />
		<a href="viewCategory?categoryId=DOGS">
			<img src="images/sm_dogs.gif" />
		</a>
		<img src="images/separator.gif" />
		<a href="viewCategory?categoryId=REPTILES">
			<img src="images/sm_reptiles.gif" />
		</a>
		<img src="images/separator.gif" />
		<a href="viewCategory?categoryId=CATS">
			<img src="images/sm_cats.gif" />
		</a>
		<img src="images/separator.gif" />
		<a href="viewCategory?categoryId=BIRDS">
			<img src="images/sm_birds.gif" />
		</a>
	</div>

</div>

<div id="Content">