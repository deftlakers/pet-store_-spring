<%--
  Created by IntelliJ IDEA.
  User: 李良才
  Date: 2020/11/14
  Time: 22:29
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="../common/IncludeTop.jsp"%>

<script type="text/javascript">
    $( document ).ready( function(){
        $('.itemShow').mouseover(function(e) {
            var n = this.name;
            // ;
            var introduction = "";
            $.ajax({
                type    : "post",
                url     : "getItemRealInfo",
                data    : {caId : n },
                dataType: "json",
                success : function (jsondata) {
                    console.log(introduction);
                    introduction += ""+jsondata.itemId+"  "+jsondata.name+"  "+jsondata.des+"  "+jsondata.price+"  "+jsondata.desc;

                    var show_div = "<pre id='show_d' style=\"border:1px,solid:#665544,width:300px,background:#efefef,padding:inherit\"> <img src=\""+jsondata.url+"\">" + introduction + "</pre>";
                    $('<pre></pre>').css({position:'absolute', fontSize: '12px' ,border: '1px' ,solid :'#665544',background: '#efefef',padding: 'inherit'} );
                    $("body").append(show_div);
                    $(this).removeAttr("title");
                    $("#show_d").css({
                        "top": (e.pageY + 30) + 'px',
                        "left": (e.pageX + 20) + "px",
                        "position": "absolute",
                        position:'absolute',border: '1px' ,solid :'#665544',background: '#efefef',padding: 'inherit'
                    }).show();
                }
            });
        }).mouseout(function() {
            $("#show_d").remove();
        });

    } );

</script>

<div id="BackLink">
    <a href="viewCategory?categoryId=${sessionScope.product.categoryId}">Return to ${sessionScope.product.categoryId}</a>
</div>

<div id="Catalog">

    <h2>${sessionScope.product.name}</h2>

    <table>
        <tr>
            <th>Item ID</th>
            <th>Product ID</th>
            <th>Description</th>
            <th>List Price</th>
            <th>&nbsp;</th>
        </tr>
        <c:forEach var="item" items="${sessionScope.itemList}">
            <tr>
                <td>
                    <a class="itemShow" href="viewItem?itemId=${item.itemId}" name="${item.itemId}">${item.itemId}</a>
                </td>
                <td>
                        ${item.product.productId}
                </td>
                <td>
                        ${item.attribute1} ${item.attribute2} ${item.attribute3}
                        ${item.attribute4} ${item.attribute5} ${sessionScope.product.name}
                </td>
                <td>
                    <fmt:formatNumber value="${item.listPrice}" pattern="$#,##0.00" /></td>
                <td>
                    <a class="Button" href="insertToCart?workingItemId=${item.itemId}">Add to Cart</a>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td>
            </td>
        </tr>
    </table>

</div>

<%@ include file="../common/IncludeBottom.jsp"%>






