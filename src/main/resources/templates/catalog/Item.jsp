<%@ include file="../common/IncludeTop.jsp"%>

<script>
	$(function() {
		var x = 20;
		var y = 20;
		$("img:not('#logoImg,#cartImg')").mouseover(function(e) {
			var src = $(this).attr("src");
			var to_body = "<div id='to_body'><img src=\"" + src + "\" id='bigImg' > </div>";
			$("body").append(to_body);
			$("#bigImg").css({
				width:"200px",
				height:"200px"
			});
			$("#to_body").css({
				"top": (e.pageY + y)*2 + "px",
				"left": (e.pageX + x)*2 + "px",
				'position': 'absolute'
			}).show();
			// 鼠标移出后，删除添加的div元素，即可使用img消失
		}).mouseout(function() {
			this.title = this.mytitle;
			$("#to_body").remove();
			// 鼠标在图片上移动时，添加的节点位置跟随鼠标变化而变化
		}).mousemove(function(e) {
			$("#to_body").css({
				"top": (e.pageY + y) + "px",
				"left": (e.pageX + x) + "px"
			});
		});
	})
</script>


<div id="BackLink">
	<a href="viewProduct?productId=${sessionScope.product.productId}">Return to ${sessionScope.product.productId}</a>
</div>

<div id="Catalog">

	<table>
		<tr>
			<td>${sessionScope.product.description}</td>
		</tr>
		<tr>
			<td><b> ${sessionScope.item.itemId} </b></td>
		</tr>
		<tr>
			<td><b><font size="4"> ${sessionScope.item.attribute1}
				${sessionScope.item.attribute2} ${sessionScope.item.attribute3}
				${sessionScope.item.attribute4} ${sessionScope.item.attribute5}
				${sessionScope.product.name} </font></b></td>
		</tr>
		<tr>
			<td>${sessionScope.product.name}</td>
		</tr>
		<tr>
			<td><c:if test="${sessionScope.item.quantity <= 0}">
				Back ordered.
			</c:if> <c:if test="${sessionScope.item.quantity > 0}">
				${sessionScope.item.quantity} in stock.
			</c:if></td>
		</tr>
		<tr>
			<td><fmt:formatNumber value="${sessionScope.item.listPrice}"
								  pattern="$#,##0.00" /></td>
		</tr>

		<tr>
			<td>
				<a class="Button" href="insertToCart?workingItemId=${item.itemId}">Add to Cart</a>
			</td>
		</tr>
	</table>

</div>

<%@ include file="../common/IncludeBottom.jsp"%>