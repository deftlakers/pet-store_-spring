<%@ include file="../common/IncludeTop.jsp"%>

<script type="text/javascript">
	$( document ).ready( function(){
		$('.productShow').mouseover(function(e) {
			var n = this.name;
			console.log(n);
			var introduction = "ItemID ProductID Description ListPrice\n";
			$.ajax({
				type    : "get",
				url     : "getItenInfo",
				data    : {caId : n },
				dataType: "json",
				success : function (data) {

					var i = 0;
					while (i  < data.length ){
						introduction += "\n"+data[i].itemId+"  "+data[i].proId+"  "+data[i].des+"  "+data[i].price;
						i++;
					}
					var show_div = "<pre id='show_d' style=\"border:1px,solid:#665544,width:300px,background:#efefef,padding:inherit\">" + introduction + "</pre>";
					$('<pre></pre>').css({position:'absolute', fontSize: '12px' ,border: '1px' ,solid :'#665544',background: '#efefef',padding: 'inherit'} );
					$("body").append(show_div);
					$(this).removeAttr("title");
					$("#show_d").css({
						"top": (e.pageY + 30) + 'px',
						"left": (e.pageX + 20) + "px",
						"position": "absolute",
						position:'absolute',border: '1px' ,solid :'#665544',background: '#efefef',padding: 'inherit'
					}).show();
				}
			});
		}).mouseout(function() {
			$("#show_d").remove();
		});

	} );

</script>



<div id="BackLink">
	<a href = "main">Return to Main Menu</a>
</div>

<div id="Catalog">

<h2>${sessionScope.category.name}</h2>

<table>
	<tr>
		<th>Product ID</th>
		<th>Name</th>
	</tr>
	<c:forEach var="product" items="${sessionScope.productList}">
		<tr>
			<td>
				<a class="productShow" href="viewProduct?productId=${product.productId}" name ="${product.productId}">${product.productId}</a>
			</td>
			<td>${product.name}</td>
		</tr>
	</c:forEach>
</table>

</div>

<%@ include file="../common/IncludeBottom.jsp"%>


