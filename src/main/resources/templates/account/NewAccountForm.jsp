<%@ include file="../common/IncludeTop.jsp"%>

<link rel="stylesheet" href="css/AutoComplete.css">

<div id="Catalog">
	<form action="register" method="post">
	<h3>User Information</h3>

	<table>
		<tr>
			<td>User ID:</td>
			<td><input id = "username" class = "usernameclass" type = "text" name = "username" />
				<span id = "usernameTips"></span>
			</td>
			<script type="text/javascript" src="${pageContext.request.contextPath }/js/Login.js"></script>
		<script>
			    var a = [];
				$.ajax({
					url : "autoComplete",
					type : "post",
					data : { code : 0},
					dataType : "json",
					success: function( jsonData ) {
						var i = 0;
						while (i  < jsonData.length ){
							a[i] = jsonData[i];
							i++;
						}
					}
				});
				$('#username').autocomplete({
					source : a
				});
		</script>

		</tr>
		<tr>
			<td>New password:</td>
			<td><input type = "text" name = "password"/></td>
		</tr>
		<tr>
			<td>Repeat password:</td>
			<td><input type = "text" name = "repeatedPassword"/></td>
		</tr>
	</table>

	<%@ include file="IncludeAccountFields.jsp"%>
	<div style="margin:0 auto;width:200px;background-color: #4d92af;
    color: white;
    padding: 8px 15px;
    border: none;
    border-radius: 4px;">
		<input type = "submit" name="newAccount" value = "Save Account Information" align="center"/>
	</div>


	</form></div>

<%@ include file="../common/IncludeBottom.jsp"%>