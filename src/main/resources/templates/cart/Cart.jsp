<%--
  Created by IntelliJ IDEA.
  User: 李良才
  Date: 2020/11/16
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: 李良才
  Date: 2020/11/16
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="../common/IncludeTop.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div id="BackLink">
    <a href="main">Return to Main Menu</a>
</div>

<div id="Catalog">

    <div id="Cart">

        <script>
            $(document).ready(function(){
                $("input:text").on("blur", function(){
                    var itemId = this.id;
                    var quantity = $("#"+itemId).val();
                    $.get("cartChange", {itemid : itemId,quantity:quantity}, function (data) {
                        $("#subTotalPrice").text("Sub Total:"+data.subTotal);
                        $("#"+itemId+"total").text(data.total);
                    })
                })

                $("[name='remove']").on("click", function () {
                    var itemId = this.id;
                    itemId = itemId.substring(0,itemId.length-6);
                    console.log(itemId);
                    $.get("removeItem", {itemId:itemId}, function (data) {
                        $("#"+itemId+"total").parent().remove();
                        $("#subTotalPrice").text("Sub Total:"+data.subTotal);

                        if (data.subTotal === "0"){
                            // $(".head").after("<tr class=\"even\"><td colspan=\"8\"><b>Your cart is empty.</b></td></tr>")
                            $("#subTotalPrice").text("Your cart is empty");
                            $("#check").remove();
                            $("#divHiden").remove();
                        }
                    })

                })
            });
        </script>


        <h2>Shopping Cart</h2>
        <form action="UpdateCart" method="post">
            <table>
                <tr>
                    <th><b>Item ID</b></th>
                    <th><b>Product ID</b></th>
                    <th><b>Description</b></th>
                    <th><b>In Stock?</b></th>
                    <th><b>Quantity</b></th>
                    <th><b>List Price</b></th>
                    <th><b>Total Cost</b></th>
                    <th>&nbsp;</th>
                </tr>

                <c:if test="${fn:length(sessionScope.cart)==0}">
                    <tr>
                        <td colspan="8"><b>Your cart is empty.</b></td>

                    </tr>
                </c:if>

                <c:forEach var="cartItem" items="${sessionScope.cart}">
                    <tr>
                        <td>
                            <a href="viewItem?itemId=${cartItem.item.itemId}" >${cartItem.item.itemId}</a>
                        </td>
                        <td id = "col2">
                                ${cartItem.item.product.productId}
                        </td>
                        <td id = "col3">
                                ${cartItem.item.attribute1} ${cartItem.item.attribute2}
                                ${cartItem.item.attribute3} ${cartItem.item.attribute4}
                                ${cartItem.item.attribute5} ${cartItem.item.product.name}
                        </td>
                        <td id = "col4">
                                ${cartItem.inStock}
                        </td>
                        <td >
                            <input type="text" id="${cartItem.item.itemId}" name = "${cartItem.item.itemId}" value="${cartItem.quantity}">
                        </td>
                        <td id = "col6">
                            <!--format标签显示单价-->
                            <fmt:formatNumber value="${cartItem.item.listPrice}"
                                              pattern="$#,##0.00" />
                        </td>
                        <td id = "${cartItem.item.itemId}total">
                            <!--format标签显示总价-->
<%--                            <fmt:formatNumber value="${cartItem.total}"--%>
<%--                                              pattern="$#,##0.00" />--%>
                            ${cartItem.total}
                        </td>
                        <td>
<%--                            removeItemFromCart?workingItemId=${cartItem.item.itemId}--%>
                            <a class="Button" name ="remove" id = "${cartItem.item.itemId}remove" >Remove</a>
                        </td>
                    </tr>
                </c:forEach>
<%--                <tr>--%>
<%--                    <td id = "${cartItem.item.itemId}subTotal" colspan="7">--%>
<%--                        Sub Total: ${sessionScope.totalCost}--%>
<%--&lt;%&ndash;                        <input type="submit" value="UpdateCart"/>&ndash;%&gt;--%>
<%--                    </td>--%>
<%--                    <td>&nbsp;</td>--%>
<%--                </tr>--%>
                <c:if test="${fn:length(sessionScope.cart)>0}">
                <tr>
                    <td id="subTotalPrice" colspan="7" style="font-weight: bold">
                        Sub Total: ${sessionScope.totalCost}
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </c:if>
            </table>
        </form>

        <c:if test="${fn:length(sessionScope.cart) > 0}">
            <div id="divHiden"style="margin:0 auto;width:200px;
    color: white;
    padding: 8px 15px;
    border: none;
    border-radius: 4px;">
            <a class="Button" id="check" href="newOrderForm?itemId=${cartItem.item.itemId}">Proceed to Checkout</a>
            </div>
         </c:if>
    </div>

    <div id="Separator">&nbsp;</div>
</div>

<%@ include file="../common/IncludeBottom.jsp"%>

