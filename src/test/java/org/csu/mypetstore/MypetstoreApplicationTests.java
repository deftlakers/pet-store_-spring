package org.csu.mypetstore;

import org.csu.mypetstore.domain.*;
import org.csu.mypetstore.persistence.CartMapper;
import org.csu.mypetstore.persistence.ProductMapper;
import org.csu.mypetstore.service.AccountService;
import org.csu.mypetstore.service.CartService;
import org.csu.mypetstore.service.CatalogService;

import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest
@MapperScan("org.csu.*")
class MypetstoreApplicationTests {

    @Autowired
    CartService cartService;
    @Autowired
    CatalogService catalogService;

    @Autowired
    AccountService accountService;
    @Test
    void contextLoads() {
    }

    @Test
    void getto(){

        Account a = new Account();
        a.setUsername("c");
        BigDecimal b = cartService.getTotal(a);
        System.out.print(b);
    }

    @Test
    void get(){
        Account a = new Account();
        a.setUsername("c");
        CartService cartService=new CartService();
        CartItem ci = new CartItem();
        Item item =  new Item();
        item.setItemId("i");
        ci.setUserid("c");
        ci.setItem(item);
//        List<CartItem> temp  = cartService.getAllItem(a);
//        CartItem temp = cartService.getItemById("i","c");
//       cartService.insertItem(ci);


        AccountService accountService = new AccountService();
        accountService.getAccount("c");
        System.out.print("2");
    }

    @Test
    void test() {
        List<Item> productList = catalogService.getAllItems();
        for(int i = 0; i < productList.size(); i++){
            System.out.println(productList.get(i).getItemName());
        }
    }

//    @Test
//    void testResetPsw(){
//        Account account = accountService.getAccount("c");
//        System.out.println(account.getPassword());
//        if (account != null){
//        System.out.println(account.getPassword());
//        accountService.resetPassword("c","00000");
//        System.out.println(account.getPassword());
//        }
//        else
//            System.out.println("null");
//
//    }


}
